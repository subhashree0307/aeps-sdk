package com.pax.pax_sdk_app;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.MPOS.BluetoothServiceActivity;
import com.matm.matmsdk.MPOS.LibActivity;
import com.matm.matmsdk.MPOS.MorefunServiceActivity;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;
import com.matm.matmsdk.aepsmodule.aadharpay.AadharpayActivity;
import com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedAepsActivity;
import com.matm.matmsdk.aepsmodule.unifiedaeps.UnifiedBioAuthActivity;
import com.matm.matmsdk.callbacks.OnFinishListener;
import com.matm.matmsdk.matm1.MatmActivity;
import com.matm.matmsdk.notification.NotificationHelper;
import com.matm.matmsdk.notification.service.SubscribeGlobal;
import com.matm.matmsdk.posverification.TransactionStatusPOSActivity;
import com.matm.matmsdk.transaction_report.TransactionStatusActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.matm.matmsdk.Utils.SdkConstants.Matm1BluetoothFlag;
import static com.matm.matmsdk.Utils.SdkConstants.balanceEnquiry;

public class MainActivity extends AppCompatActivity implements OnFinishListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry, rb_mini, rb_adhaarpay, rb_pos;
    EditText etAmount, et_paramA, et_paramB, et_mobileNumber;
    Button btn_download, btnProceed, btn_pair, btn_proceedaeps, upiPagebtn, BtnUnpair, Btndriver, btn_matm1, btnmatm1pair, btn_check, btn_pos;
    public static final int REQUEST_CODE = 5;
    Boolean isAepsClicked = false, isMatmClicked = false;
    String manufactureFlag = "";
    UsbManager musbManager;
    private UsbDevice usbDevice;
    ProgressDialog pd;
    public static String token, adminName;
    ProgressDialog dialog;
    String username = "Snehasony";
    NotificationHelper notificationHelper;
    String[] permissions = {"android.permission.BLUETOOTH", "android.permission.BLUETOOTH_ADMIN", "android.permission.BLUETOOTH_SCAN", "android.permission.BLUETOOTH_CONNECT"};
    Boolean isBTPermission = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        //  FirebaseApp.initializeApp(MainActivity.this);
        notificationHelper = new NotificationHelper(this);
        subscribeGlobal();
        //retriveUserList();
        String str = "609384625620";
        String hashString = getSha256Hash(str);
        System.out.println("String Value :" + hashString);

        Date date = Calendar.getInstance().getTime();

        //For Bluetooth Permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, 80);
        }

        // Display a date in day, month, year format
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String currentDateandTime = formatter.format(date);

        Log.e(TAG, "subscribeGlobal: current date " + currentDateandTime);

    }


    public void subscribeGlobal() {
        String specialChar = "!@#$%^&*";

        if (SdkConstants.USER_NAME_NOTIFY.matches(specialChar)) {
            SdkConstants.DEVICE_TOPIC = "";
        } else {
            SdkConstants.DEVICE_TOPIC = "AEPS_Snehasony";//+SdkConstants.loginID;
        }

        SdkConstants.COMPLETE_REGISTRATION = "registrationComplete";
        SdkConstants.PUSH_NOTIFICATION = "pushNotification";

        Log.d("TAG", "subscribeGlobal: " + SdkConstants.DEVICE_TOPIC);
        SubscribeGlobal global = new SubscribeGlobal(this);
        global.subscribe();
        global.registerBroadcast();

    }


    private void initView() {
        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        btn_pair = findViewById(R.id.btn_pair);
        rb_mini = findViewById(R.id.rb_mini);
        rb_adhaarpay = findViewById(R.id.rb_adhaarpay);
        rb_pos = findViewById(R.id.rb_pos);
        etAmount = findViewById(R.id.et_amount);
        et_mobileNumber = findViewById(R.id.et_mobileNumber);
        btn_proceedaeps = findViewById(R.id.proceedBtn);
        upiPagebtn = findViewById(R.id.upiPagebtn);
        BtnUnpair = findViewById(R.id.BtnUnpair);
        Btndriver = findViewById(R.id.Btndriver);
        btn_download = findViewById(R.id.btn_download);
        btn_pos = findViewById(R.id.btn_pos);
//        btn_check = findViewById(R.id.btn_check);


//        loginToken();


        BtnUnpair = findViewById(R.id.BtnUnpair);
        btn_matm1 = findViewById(R.id.btn_matm1);
        btnmatm1pair = findViewById(R.id.btnmatm1pair);

        btn_matm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SdkConstants.transactionAmount = etAmount.getText().toString().trim();
                if (rbCashWithdrawal.isChecked()) {
                    SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                    SdkConstants.transactionAmount = etAmount.getText().toString();

                }
                if (rbBalanceEnquiry.isChecked()) {
                    SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                    SdkConstants.transactionAmount = "0";
                }
                SdkConstants.loginID = "9425300691";
                SdkConstants.encryptedData = "mmBIDCjHcOhhFmWOTEBKkOawKi3GsmjsB%2B1BUhKfk%2BPf71PNyEZQCeqS%2BIphX5v9pH697R2my7mulqff1m4dFwcyN74ZpYIiui58QdwjE7IPuPeS409xiDhyuHuHie66";


                Intent intent = new Intent(MainActivity.this, MatmActivity.class);
                startActivity(intent);
            }
        });

        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SdkConstants.BRAND_NAME="BLS PAY";
                SdkConstants.SHOP_NAME= "iServeU";
                Intent intent = new Intent(MainActivity.this, TransactionStatusPOSActivity.class);
                intent.putExtra("flag", "success");
                intent.putExtra("TRANSACTION_ID", "6656268036677632");
                intent.putExtra("TRANSACTION_TYPE", "Cash");
                intent.putExtra("TRANSACTION_AMOUNT", "10000");
                intent.putExtra("RRN_NO", "222012065608");
                intent.putExtra("RESPONSE_CODE", "57");
                intent.putExtra("APP_NAME", "RuPay Debit");
                intent.putExtra("AID", "A0000000031010");
                intent.putExtra("AMOUNT", "100");
                intent.putExtra("MID", "345364373");
                intent.putExtra("TID", "10687348");
                intent.putExtra("TXN_ID", "1234567");
                intent.putExtra("INVOICE", "1111111");
                intent.putExtra("CARD_TYPE", "rupay");
                intent.putExtra("APPR_CODE", "123456");
                intent.putExtra("merchant_name", "itpl");
                intent.putExtra("appl_preferred_name", "testing");
                intent.putExtra("terminal_result", "4532");
                intent.putExtra("dedicated_file_name", "test");
                intent.putExtra("txn_status", "");
                intent.putExtra("application_cryptogram", "teshgsavhv");
                intent.putExtra("application_version", "");
                intent.putExtra("pin", "PIN verified OK, Signature not required.");
                intent.putExtra("card_name", "GYANA RANJAN BEHERA   /");
                intent.putExtra("CARD_HOLDERNAME", "GYANA RANJAN BEHERA   /");
                intent.putExtra("rrn", "RRN:224915090271  AUTH ID:610689");
                intent.putExtra("card_type", "RUPAY");
                intent.putExtra("card", "xxxx-xxxx-xxxx-5432");
                intent.putExtra("sale", "");
                intent.putExtra("batch", "BATCH NO:220906    INV NR:6656268036677632");
                intent.putExtra("mid_tid", "MID:107293000707786 TID:10697330");
                intent.putExtra("location", "bbsr");
                intent.putExtra("bank_name", "icici bank");
                intent.putExtra("CARD_NUMBER", "xxxx-xxxx-xxxx-5432");
                intent.putExtra("status_code", "NMBEC_55 -  Incorrect Personal Identification Number");
                startActivity(intent);
            }
        });


        btnmatm1pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Matm1BluetoothFlag = "1";
                Intent intent = new Intent(MainActivity.this, MatmActivity.class);
                startActivity(intent);
            }
        });


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SdkConstants.BlueToothPairFlag.equalsIgnoreCase("1")) {
//                    if (PosActivity.isBlueToothConnected(MainActivity.this)) {

                    if (rbCashWithdrawal.isChecked()) {
                        String Amount = etAmount.getText().toString().trim();
                        if (!Amount.equals("")) {
                            if (Integer.parseInt(Amount) >= 100) {
                                callMATMSDKApp();
                            } else {
                                Toast.makeText(MainActivity.this, "Please Enter Amount more Than 100", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Please Enter some Amount", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (rbBalanceEnquiry.isChecked()) {
                        callMATMSDKApp();
                    }

//                    } else {
//                        Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please pair bluetooth device for ATM transaction.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_proceedaeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(biometricDeviceConnect()){
                if (isBTPermission) {
                    callAEPSSDKApp();
                }else {
                    showExitDialog();
                }
//                }else{
//                    Toast.makeText(MainActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        btn_pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BluetoothServiceActivity.class);
                intent.putExtra("userName", "Snehasony");
                intent.putExtra("user_token", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTbmVoYXNvbnkiLCJhdWRpZW5jZSI6IndlYiIsImNyZWF0ZWQiOjE2MjA3MjM3Njc2NDYsImV4cCI6MTYyMDcyNTU2N30.--MAMha1Ab70x89FjBD4kyu3EgbsqJl0N9gHG-HlM-caQ8gY2UX3ULX0fapgkReOEug32HlKtcSz5TG7Ec-YMA");
                Log.d("TAG", "token_pair: " + token);
                SdkConstants.applicationType = "CORE";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    } else {
                        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }
                } else {
                    startActivity(intent);
                }
            }
        });

        btn_pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SdkConstants.BlueToothPairFlag.equalsIgnoreCase("1")) {
//                    if (PosActivity.isBlueToothConnected(MainActivity.this)) {
                    if (rb_pos.isChecked()) {
                        String Amount = etAmount.getText().toString().trim();
                        String mob = et_mobileNumber.getText().toString().trim();
                        callMATMSDKApp();
                        /*if (!Amount.equals("")) {
                            if (Integer.parseInt(Amount) >= 100) {
                                if(!mob.equals("")){
                                    callMATMSDKApp();
                                }else{
                                    Toast.makeText(MainActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Please Enter Amount more Than 100", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Please Enter some Amount", Toast.LENGTH_SHORT).show();
                        }*/
                    }

//                    } else {
//                        Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please pair bluetooth device for ATM transaction.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        BtnUnpair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SdkConstants.LogOut = "0";
                SdkConstants.BlueToothPairFlag = "0";
                Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                intent.putExtra("user_id", "488");
                intent.putExtra("user_token", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkzMjcwOTQzNzcwLCJleHAiOjE1OTMyNzI3NDN9.U6OHRnz-UvkcDOicmUYm_yT8FSDofbzZ9H8kJsLdLwgTRwt5vJ0PIwTKYC1JRpM3kXwqGg6sCpvQn1PmUz9lgw");

                startActivity(intent);
            }
        });


        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_cw) {
                    etAmount.setClickable(true);
                    etAmount.setHint("Amount");
                    etAmount.setVisibility(View.VISIBLE);
                    etAmount.setText("");
                    etAmount.setEnabled(true);
                    etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else if (checkedId == R.id.rb_be) {
                    etAmount.setVisibility(View.GONE);
                    etAmount.setClickable(false);
                    etAmount.setEnabled(false);
                } else if (checkedId == R.id.rb_adhaarpay) {
                    etAmount.setClickable(true);
                    etAmount.setHint("Amount");
                    etAmount.setVisibility(View.VISIBLE);
                    etAmount.setText("");
                    etAmount.setEnabled(true);
                    etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else if (checkedId == R.id.rb_mini) {
                    etAmount.setVisibility(View.GONE);
                    etAmount.setClickable(false);
                    etAmount.setEnabled(false);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null & resultCode == RESULT_OK) {

            /**
             *  FOR AEPS TRANSACTION RESPONSE
             */
            if (requestCode == SdkConstants.REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response: " + response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
            /**
             *  FOR MATM TRANSACTION RESPONSE
             */
            if (requestCode == SdkConstants.MATM_REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response: " + response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
            if(requestCode==1000){
                isBTPermission=true;
            }
        }
        if(requestCode==1000){
            isBTPermission=true;
        }
    }


      /*  SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if (rbCashWithdrawal.isChecked()) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;

        } else if(rbBalanceEnquiry.isChecked()) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
        } else {

            Toast.makeText(this,"Please select any transaction type !!!",Toast.LENGTH_LONG);
        }

        SdkConstants.paramA = "Subhalaxmi";
        SdkConstants.paramB = "1111111";
        SdkConstants.paramC = "123456";

        //SdkConstants.applicationType = "CORE";
        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTbmVoYXNvbnkiLCJhdWRpZW5jZSI6IndlYiIsImNyZWF0ZWQiOjE2MDQ5MTA5NDg2NTQsImV4cCI6MTYwNDkxMjc0OH0.BWEErMERTGt0LA186C1eJ0xdm_AdmPgDNzB5Xbn55MHCBDJptWW6JGhkBcQYTBLijfJPE87ZwZWsFYNQ6GN5KA";
        SdkConstants.userNameFromCoreApp = "Snehasony";
        SdkConstants.CustomTheme = "THEME_YELLOW";

        //THEME_YELLOW
        //THEME_BLUE
        //THEME_DARK
        //THEME_RED
        //THEME_BROWN
        //THEME_GREEN
        //DEFAULT


//        SdkConstants.loginID = "jhunjhunuap.2512";
//        SdkConstants.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVrOZefRP0s10rREkrzYvxahvS4SiPYqnTY3R3MgarVjnyvrz3mjOEb%2F261GisLVNYQ%3D%3D";

        //annarpurna
//        SdkConstants.loginID = "aepsTestR";
//        SdkConstants.encryptedData ="cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAP6rabeaskLloqjx6bF6tCcw%3D%3D";
        //instantmudra
//        SdkConstants.loginID = "7003585693";
//        SdkConstants.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVpG%2BMXYkFjv7FvFq5zSop426IBfOKVTFtcsgZDUCORAu%2FDJvr85SGAUeQVWgRINTI5teZqYzUL1nyFMcf1eO69A%3D";


        System.out.println("Rajesh::::" + manufactureFlag);
        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        SdkConstants.DRIVER_ACTIVITY = "com.pax.pax_sdk_app.DriverActivity";

        Intent intent = new Intent(MainActivity.this, AEPS2HomeActivity.class);
        //intent.putExtra("activity","com.pax.pax_sdk_app.DriverActivity");
        //intent.putExtra("driverFlag",manufactureFlag);
        startActivity(intent);*/
    //}

    private void callAEPSSDKApp() {
        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if (rbCashWithdrawal.isChecked()) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
        } else if (rbBalanceEnquiry.isChecked()) {

            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
        } else if (rb_mini.isChecked()) {
            SdkConstants.transactionType = SdkConstants.ministatement;
        } else if (rb_adhaarpay.isChecked()) {
            SdkConstants.transactionType = SdkConstants.adhaarPay;
        }
        SdkConstants.paramA = "test";
        SdkConstants.paramB = "BLS1";
        SdkConstants.paramC = "loanID";
        SdkConstants.applicationType = "CORE";
//             bearer token
//        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJhbm5peWFtLWFlcHNAY3JlZGl0YXBwLTI5YmYyLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiMTE1OTQyODI3NDI0OTQzNTQzNjE4IiwiZXhwIjoxNjkyOTcyNzk1LCJpYXQiOjE2NjE0MzY3OTUsInN1YiI6ImFubml5YW0tYWVwc0BjcmVkaXRhcHAtMjliZjIuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJlbWFpbCI6ImFubml5YW0tYWVwc0BjcmVkaXRhcHAtMjliZjIuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20ifQ.WTMYIq6O8p_Dqyg1XrLt1LYCyXQBfPCvXUGW8G4irj7X7nCsEOVfgnjymLfETQNLLTHnmVdS-J4BW9SLqYQDsIv2opPAZVzHxRom3NNWZZfLhBhWUobTyq8it5wh4VQ8osCFYmFHZzlRYpiBY-xFaFXUB1LULAQizm95LtqrinyOVTc1p0w1nQc4LQiPQCtGfvfufadtaVF4Y4z4UMCmzyppOGjH_eh-O9_-AyHN72Au9oF2_277dfLnlu0JmPWwr56EKtUkboLqe77xSiR-sUl_Nj-Zsn1hcLbt5xRo4V8Ebnd_QQunXjXOojNmXvubmd6Zzm9irZdyJzhA9FjDMQ";
//        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNjYxMzM2MDc5NzgwLCJleHAiOjE2NjEzMzk2Nzl9.aXdG__4GP6agfhooT4MB95jDu2aHd36XbPrKx5mVro2RyeaFAYdd58F_EGu3kyA7GwUfqFblOVF42C0ZjH4OKQ";
        SdkConstants.tokenFromCoreApp = getIntent().getStringExtra("token");
//        SdkConstants.userNameFromCoreApp = "Anniyamtest";
        SdkConstants.userNameFromCoreApp = getIntent().getStringExtra("username");
        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        SdkConstants.DRIVER_ACTIVITY = "com.pax.pax_sdk_app.DriverActivity";
        SdkConstants.BRAND_NAME = "";
        SdkConstants.SHOP_NAME="";
//        SdkConstants.internalFPName = "wiseasy";
        SdkConstants.API_USER_NAME_VALUE = "";

//        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJldmFibGVhcGktYWVwc0BjcmVkaXRhcHAtMjliZjIuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJhdWQiOiIxMDE0ODE3OTgyODg5NTc3NjA3MjQiLCJleHAiOjE2ODkyMjc4MjMsImlhdCI6MTY1NzY5MTgyMywic3ViIjoiZXZhYmxlYXBpLWFlcHNAY3JlZGl0YXBwLTI5YmYyLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiZW1haWwiOiJldmFibGVhcGktYWVwc0BjcmVkaXRhcHAtMjliZjIuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20ifQ.O6yiA-imUOaVb5uFV2eD7_hkRdwjw4Z855FBZa-kVyw-rEqToFMlx3SjBU-iJVIOLxGVAyqdlVpwI2erP9h70BShov9h6b3mjfr_WWEai7HWPXhtwddEUJgoO79TnoFnKEeYPTbQxV3l84JWo8vMJM7fs9CDRrigBcYtMLh_bQtZlm-vXE0FAj6XZiAqpr1KracCn5118GkMeEvSfIG8DC5yLIF7CQ-HJN9sYdLfba-Jm0AD-Oaliyh0XVRNxKAB2J6vsY-WgT8p55rL1yApmLzr-tVjRL_zb2UWKgfOsrTyJLYMuJqKRqMIcYsignxnpMUz5Ro7TKOMQ-y8DD8qkw";
//        SdkConstants.userNameFromCoreApp = "Sagarikatest";
//        SdkConstants.API_USER_NAME_VALUE = "";
        Intent intent = new Intent(MainActivity.this, UnifiedBioAuthActivity.class);
        startActivity(intent);
    }

    private void callMATMSDKApp() {
        rb_mini.setVisibility(View.GONE);
//        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if (rbCashWithdrawal.isChecked()) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = etAmount.getText().toString();

        }
        if (rbBalanceEnquiry.isChecked()) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "0";
        }
        if (rb_pos.isChecked()) {
            SdkConstants.transactionType = "POS";
            SdkConstants.transactionAmount = "0";
        }
        SdkConstants.paramA = "123456789";
        SdkConstants.paramB = "branch1";
        SdkConstants.paramC = "loanID1234";
        SdkConstants.USER_MOBILE_NO = "7978628756";
//        SdkConstants.encryptedData ="cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAP6rabeaskLloqjx6bF6tCcw%3D%3D";
//        SdkConstants.loginID = "aepsTestR";
        Log.d("TAG", "token_callMATMSDKApp: " + token);
        SdkConstants.applicationType = "CORE";
        SdkConstants.IS_BETA_USER = true;

        SdkConstants.userNameFromCoreApp = getIntent().getStringExtra("username");
//        SdkConstants.userNameFromCoreApp = "itpl";
//        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNjY0MTg0MDYyNDAxLCJleHAiOjE2NjQxODc2NjJ9.SoX-GHgx26_GFfS32w-9Ye8VJ2XuXR21g2xla9PKquOKk3qy4HkK7fyyCzJlK92mtsrDXZFaksJxsdi_slJquQ";
        SdkConstants.DEVICE_TYPE = "pax";
        SdkConstants.API_USER_NAME_VALUE = "";


        //        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTbmVoYXNvbnkiLCJhdWRpZW5jZSI6IndlYiIsImNyZWF0ZWQiOjE2MjAxMjY0NzQxMjEsImV4cCI6MTYyMDEyODI3NH0.yyg3e1bdPY9E2ZPEqYKuOP-CGwoVo3GrTHj4lupv-vzom-mWJ2bQEn7UZPURA4dslHwTiis8_tpixqkOyFlnIw";
//        SdkConstants.tokenFromCoreApp = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJTbmVoYXNvbnkiLCJhdWRpZW5jZSI6IndlYiIsImNyZWF0ZWQiOjE2MDYzOTAwMzkwODcsImV4cCI6MTYwNjM5MTgzOX0.k3x0n6OSIPwzWdrIxBdQS1U9jXHu0I7SLHMaV81fN_HuFJ83f7EwSYVn38lYDgh6g8mBgJroo38-MH89aROW3g";
//        SdkConstants.tokenFromCoreApp = token;
        //SdkConstants.loginID = "Bindlish.a.11250";
        //SdkConstants.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVrOZefRP0s10rREkrzYvxahvS4SiPYqnTY3R3MgarVjnyvrz3mjOEb%2F261GisLVNYQ%3D%3D";
        Intent intent = new Intent(MainActivity.this, MorefunServiceActivity.class);
        startActivityForResult(intent, SdkConstants.MATM_REQUEST_CODE);
    }


    @Override
    protected void onResume() {
        String str = SdkConstants.responseData;
//        Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
        super.onResume();
    }


    public String getSha256Hash(String password) {
        try {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            }
            digest.reset();
            return bin2hex(digest.digest(password.getBytes()));
        } catch (Exception ignored) {
            return null;
        }
    }

    private String bin2hex(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b : data)
            hex.append(String.format("%02x", b & 0xFF));
        return hex.toString();
    }

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                    }
                })
                .show();
    }

    private void loginToken() {
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        String url = "https://itpl.iserveu.tech/generate/v1";
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String hello = response.getString("hello");
                            byte[] data = Base64.decode(hello, Base64.DEFAULT);
                            String base64 = "";
                            try {
                                base64 = new String(data, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            generateToken(base64);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });
    }

    private void generateToken(String url) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", username);
            obj.put("password", "Password@1");

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                token = response.getString("token");
                                adminName = response.getString("adminName");
                                Toast.makeText(MainActivity.this, "Token Generated", Toast.LENGTH_SHORT).show();

                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.dismiss();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            dialog.dismiss();
        }

    }

    public String getDecimalString(String s) {
        String d = "N/A";
        if (s != null && !s.contains(" ") && !s.equals("")) {
            if (s.contains(".")) {
                int index = s.lastIndexOf(".");
                int size = s.length() - 1;
                if (index == size) {
                    d = s + "00";
                } else if (index == size - 1) {
                    d = s + "0";
                } else {
                    d = s;
                }
            } else {
                d = s + ".00";
            }
        }
        return d;
    }

    @Override
    public void onSDKFinish(String statusTxt, String paramA, String statusDesc) {
        Toast.makeText(MainActivity.this, statusTxt + paramA + statusDesc, Toast.LENGTH_SHORT).show();

        Log.e(TAG, "status .." + statusTxt);
        Log.e(TAG, "paramA.." + paramA);
        Log.e(TAG, "status desc.." + statusDesc);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 80) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED || grantResults[1] == PackageManager.PERMISSION_GRANTED || grantResults[2] == PackageManager.PERMISSION_GRANTED || grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                isBTPermission = true;
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void showExitDialog() {
        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("" +
                "\nClick SETTINGS to Manually Set\n" + "Permissions to access the App");
        builder1.setCancelable(false);
        builder1.setTitle("Change Permissions in  Settings");
        builder1.setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, 1000);     // Comment 3.
            }
        });
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        android.app.AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
